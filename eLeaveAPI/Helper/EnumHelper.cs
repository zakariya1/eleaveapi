﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace eLeaveAPI.Helper
{
    public class EnumHelper
    {
        public enum Refer
        {
            Hiliday = 1,
            Personal = 2,
            Sick = 3,
        }
        public enum Status
        { 
            Request = 0,
            Approve = 1,
            Reject = 2,
            Cancel = 3,
        }
        //public enum EventType
        //{
        //    ////[Display(Name = "Morning")]
        //    //Morning = "TEST".ToString(),
        //    ////[Display(Name = "Afternoon")]
        //    //Afternoon = TE,
        //    ////[Display(Name = "Full Day")]
        //    //Fullday = 'sdfsdfsd'

        //}
        public static class EventType
        {
            public const string Morning = "Morning";
            public const string Afternoon = "Afternoon";
            public const string Fullday = "Full Day";
        }
    }
}
